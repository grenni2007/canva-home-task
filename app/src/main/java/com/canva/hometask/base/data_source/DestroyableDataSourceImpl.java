package com.canva.hometask.base.data_source;

import android.support.annotation.CallSuper;
import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class DestroyableDataSourceImpl implements DestroyableDataSource {
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void cancel() {
        if (compositeDisposable.size() > 0) {
            compositeDisposable.dispose();
            compositeDisposable = new CompositeDisposable();
        }
    }

    private void removeDisposable(Disposable disposable) {
        compositeDisposable.remove(disposable);
    }

    private void addDisposable(Disposable d) {
        compositeDisposable.add(d);
    }

    protected abstract class DisposableObserver<T> implements Observer<T> {
        private Disposable disposable;

        @Override
        @CallSuper
        public void onSubscribe(Disposable d) {
            Log.d("Mosaic", "DisposableObserver onSubscribe, thread =" + Thread.currentThread().getName());
            disposable = d;
            addDisposable(d);
        }

        @Override
        @CallSuper
        public void onNext(T value) {
            Log.d("Mosaic", "DisposableObserver onNext, thread =" + Thread.currentThread().getName());
        }

        @Override
        @CallSuper
        public void onError(Throwable e) {
            Log.d("Mosaic", "DisposableObserver onError, thread =" + Thread.currentThread().getName());
            removeDisposable(disposable);
        }

        @Override
        @CallSuper
        public void onComplete() {
            Log.d("Mosaic", "DisposableObserver onComplete, thread =" + Thread.currentThread().getName());
            removeDisposable(disposable);
        }
    }
}
