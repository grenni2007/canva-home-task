package com.canva.hometask.base.data_source;

public interface DestroyableDataSource {
    void cancel();
}
