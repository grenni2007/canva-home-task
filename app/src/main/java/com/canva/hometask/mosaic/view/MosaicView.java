package com.canva.hometask.mosaic.view;

import android.support.annotation.StringRes;

import com.canva.hometask.mosaic.widget.MosaicGridView;

public interface MosaicView {
    MosaicView STUB = new MosaicView() {
        @Override
        public void initView(Listener listener) {

        }

        @Override
        public void updateMosaicView(int width, int height, MosaicGridView.Adapter modelAdapter) {

        }

        @Override
        public void resetView() {

        }

        @Override
        public int getItemWidth() {
            return 0;
        }

        @Override
        public int getItemHeight() {
            return 0;
        }

        @Override
        public int getMosaicViewWidth() {
            return 0;
        }

        @Override
        public int getMosaicViewHeight() {
            return 0;
        }

        @Override
        public void showError(@StringRes int errorStringResId) {

        }

        @Override
        public void notifyRowChanged(int i) {

        }
    };

    void initView(Listener listener);

    void updateMosaicView(int width, int height, MosaicGridView.Adapter modelAdapter);

    void resetView();

    int getItemWidth();

    int getItemHeight();

    int getMosaicViewWidth();

    int getMosaicViewHeight();

    void showError(@StringRes int errorStringResId);

    void notifyRowChanged(int i);

    interface Listener {
        void onSelectImageClicked();
    }
}
