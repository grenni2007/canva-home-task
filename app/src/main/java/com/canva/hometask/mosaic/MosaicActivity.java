package com.canva.hometask.mosaic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.canva.hometask.mosaic.di.DaggerMosaicComponent;
import com.canva.hometask.mosaic.di.MosaicModule;
import com.canva.hometask.mosaic.presenter.MosaicPresenter;

import javax.inject.Inject;

public class MosaicActivity extends AppCompatActivity {

    @Inject
    MosaicPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerMosaicComponent.builder()
                .mosaicModule(new MosaicModule(this))
                .build()
                .inject(this);
        presenter.handleOnCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!presenter.handleOnActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.handleSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.handleOnDestroy();
    }
}
