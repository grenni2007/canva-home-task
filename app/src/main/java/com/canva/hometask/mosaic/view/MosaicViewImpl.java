package com.canva.hometask.mosaic.view;

import android.app.Activity;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.canva.hometask.R;
import com.canva.hometask.mosaic.widget.MosaicGridView;

import javax.inject.Inject;

public class MosaicViewImpl implements MosaicView {
    private final Activity activity;
    private MosaicGridView mosaicGridView;

    @Inject
    public MosaicViewImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void initView(final Listener listener) {
        activity.setContentView(R.layout.activity_main);
        mosaicGridView = (MosaicGridView) activity.findViewById(R.id.mosaic_view);
        activity.findViewById(R.id.select_image_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelectImageClicked();
            }
        });
    }

    @Override
    public void updateMosaicView(int width, int height, MosaicGridView.Adapter modelAdapter) {
        mosaicGridView.setLayoutParams(new LinearLayout.LayoutParams(
                width,
                height));
        mosaicGridView.setAdapter(modelAdapter);
    }

    @Override
    public void resetView() {
        mosaicGridView.setAdapter(null);
        mosaicGridView.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    public int getItemWidth() {
        return activity.getResources().getDimensionPixelSize(R.dimen.mosaic_item_width);
    }

    @Override
    public int getItemHeight() {
        return activity.getResources().getDimensionPixelSize(R.dimen.mosaic_item_height);
    }

    @Override
    public int getMosaicViewWidth() {
        return mosaicGridView.getWidth();
    }

    @Override
    public int getMosaicViewHeight() {
        return mosaicGridView.getHeight();
    }

    @Override
    public void showError(@StringRes int errorStringResId) {
        Toast.makeText(activity, errorStringResId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void notifyRowChanged(int i) {
        mosaicGridView.getAdapter().notifyRowChanged(i);
    }
}
