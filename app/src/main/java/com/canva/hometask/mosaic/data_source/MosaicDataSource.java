package com.canva.hometask.mosaic.data_source;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;

import com.canva.hometask.base.data_source.DestroyableDataSource;

public interface MosaicDataSource extends DestroyableDataSource {
    MosaicDataSource STUB = new MosaicDataSource() {
        @Override
        public void loadItems(Bitmap bitmap, int rowCount, int columnCount, int itemWidth, int itemHeight, Listener listener) {

        }

        @Override
        public int getRowCount() {
            return 0;
        }

        @Override
        public int getColumnCount() {
            return 0;
        }

        @Override
        public SparseArray<Drawable>[] getDrawables() {
            return new SparseArray[0];
        }

        @Override
        public void cancel() {

        }
    };

    void loadItems(Bitmap bitmap,
                   int rowCount, int columnCount,
                   int itemWidth, int itemHeight,
                   Listener listener);

    int getRowCount();

    int getColumnCount();

    SparseArray<Drawable>[] getDrawables();

    interface Listener {
        void onRawDataChanged(int i);

        void onDataItemLoadError();
    }
}
