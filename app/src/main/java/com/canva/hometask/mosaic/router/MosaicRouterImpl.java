package com.canva.hometask.mosaic.router;

import android.app.Activity;
import android.content.Intent;

import javax.inject.Inject;

public class MosaicRouterImpl implements MosaicRouter {
    private final Activity activity;

    @Inject
    public MosaicRouterImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void openSelectImageScreen(int requestCode) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        activity.startActivityForResult(intent, requestCode);
    }
}
