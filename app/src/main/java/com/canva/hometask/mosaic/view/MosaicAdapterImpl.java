package com.canva.hometask.mosaic.view;

import android.graphics.drawable.Drawable;
import android.util.SparseArray;

import com.canva.hometask.mosaic.data_source.MosaicDataSource;
import com.canva.hometask.mosaic.widget.MosaicGridView;

public class MosaicAdapterImpl extends MosaicGridView.Adapter {
    private final MosaicDataSource interactor;

    public MosaicAdapterImpl(MosaicDataSource interactor) {
        this.interactor = interactor;
    }

    @Override
    public int getRowCount() {
        return interactor.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return interactor.getColumnCount();
    }

    @Override
    public Drawable getDrawable(int rowIndex, int columnIndex) {
        SparseArray<Drawable>[] drawables = interactor.getDrawables();
        if (drawables[rowIndex].size() == getColumnCount()) {
            return drawables[rowIndex].get(columnIndex);
        } else {
            return null;
        }
    }

    @Override
    protected boolean hasAnyDrawables() {
        for (SparseArray array : interactor.getDrawables()) {
            if (array.size() != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean isRawHasAllDrawables(int row) {
        return interactor.getDrawables()[row].size() == getColumnCount();
    }
}
