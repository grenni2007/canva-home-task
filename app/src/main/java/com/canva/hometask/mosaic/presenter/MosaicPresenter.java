package com.canva.hometask.mosaic.presenter;

import android.content.Intent;
import android.os.Bundle;

public interface MosaicPresenter {
    void handleOnCreate(Bundle savedInstanceState);

    boolean handleOnActivityResult(int requestCode, int resultCode, Intent data);

    void handleOnDestroy();

    void handleSaveInstanceState(Bundle outState);
}
