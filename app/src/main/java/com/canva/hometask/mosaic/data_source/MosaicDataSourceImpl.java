package com.canva.hometask.mosaic.data_source;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.util.SparseArray;

import com.canva.hometask.base.data_source.DestroyableDataSourceImpl;
import com.canva.hometask.mosaic.util.Palette;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MosaicDataSourceImpl extends DestroyableDataSourceImpl implements MosaicDataSource {

    private Listener listener;
    private SparseArray<Drawable>[] items;
    private int rowCount;
    private int columnCount;

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    @Nullable
    public SparseArray<Drawable>[] getDrawables() {
        return items;
    }

    @Override
    public void loadItems(final Bitmap bitmap,
                          final int rowCount, final int columnCount,
                          final int itemWidth, final int itemHeight,
                          final Listener listener) {
        this.listener = listener;
        this.columnCount = columnCount;
        this.rowCount = rowCount;
        initItems();
        ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        Scheduler scheduler = Schedulers.from(executor);
        createRequestItemsObservable(rowCount, columnCount, itemWidth, itemHeight)
                .subscribeOn(scheduler)
                .map(new Function<RequestItem, ResultItem>() {
                    @Override
                    public ResultItem apply(RequestItem mosaicRequestItem) throws Exception {
                        return getMosaicItem(bitmap, mosaicRequestItem);
                    }
                })
                .buffer(columnCount / 2)
                .subscribeOn(scheduler)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ItemsObserver());
    }

    @SuppressWarnings("unchecked")
    private void initItems() {
        items = new SparseArray[rowCount];
        for (int i = 0; i < rowCount; i++) {
            items[i] = new SparseArray<>();
        }
    }

    private Observable<RequestItem> createRequestItemsObservable(final int rowCount, final int columnCount,
                                                                 final int itemWidth, final int itemHeight) {
        return Observable
                .create(new ObservableOnSubscribe<RequestItem>() {
                    @Override
                    public void subscribe(ObservableEmitter<RequestItem> e) throws Exception {
                        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
                            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                                e.onNext(getMosaicRequestItem(
                                        rowIndex, columnIndex,
                                        itemHeight, itemWidth));
                            }
                        }
                        e.onComplete();
                    }
                });
    }

    @NonNull
    private RequestItem getMosaicRequestItem(int rowIndex, int columnIndex, int itemHeight, int itemWidth) {
        int top = itemHeight * rowIndex;
        int left = itemWidth * columnIndex;
        int right = left + itemWidth - 1;
        int bottom = top + itemHeight - 1;
        Rect rect = new Rect(left, top, right, bottom);
        return new RequestItem(rowIndex, columnIndex, rect);
    }

    private ResultItem getMosaicItem(Bitmap bitmap, RequestItem item) throws IOException {
        int averageColor = Palette.generate(bitmap, item.rect).getAverageColor();
        Drawable drawable = getMosaicItemDrawable(item, averageColor);
        return new ResultItem(item, drawable);
    }

    private Drawable getMosaicItemDrawable(RequestItem item, int averageColor) throws IOException {
//        return createMosaicDrawable(item.rect.width(), item.rect.height(), averageColor);
        return getDrawableFromServer(item.rect.width(), item.rect.height(), averageColor);
    }

    private Drawable getDrawableFromServer(int width, int height, int color) throws IOException {
        String hexColor = String.format("%06X", (0xFFFFFF & color));
        String uriString = new Uri.Builder()
                .scheme("http")
                .encodedAuthority("10.0.2.2:8765")
                .appendPath("color")
                .appendPath(String.valueOf(width))
                .appendPath(String.valueOf(height))
                .appendPath(hexColor)
                .build()
                .toString();
        URLConnection urlConnection = new URL(uriString).openConnection();
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        Bitmap bitmap = BitmapFactory.decodeStream(in);
        if (bitmap == null) {
            return null;
        } else {
            return new BitmapDrawable(bitmap);
        }
    }

    @SuppressWarnings("unused")
    private Drawable createMosaicDrawable(int width, int height, int color) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(color);
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(null, bitmap);
        drawable.setCornerRadius(Math.max(width / 2, height / 2));
        return drawable;
    }

    private void onItemsLoaded(List<ResultItem> items) {
        Set<Integer> changedRowIndexes = new HashSet<>();
        for (ResultItem item : items) {
            int rowIndex = item.requestItem.rowIndex;
            int columnIndex = item.requestItem.columnIndex;
            this.items[rowIndex].put(columnIndex, item.drawable);
            changedRowIndexes.add(rowIndex);
        }
        notifyRowsChanged(changedRowIndexes);
    }

    private void notifyRowsChanged(Set<Integer> indexes) {
        if (listener != null) {
            for (Integer rowIndex : indexes) {
                listener.onRawDataChanged(rowIndex);
            }
        }
    }

    private void onDataLoadError() {
        listener.onDataItemLoadError();
    }

    @Override
    public void cancel() {
        super.cancel();
        listener = null;
        items = null;
        columnCount = 0;
        rowCount = 0;
    }

    private static class RequestItem {
        private final int rowIndex;
        private final int columnIndex;
        private final Rect rect;

        RequestItem(int rowIndex, int columnIndex, Rect rect) {
            this.rowIndex = rowIndex;
            this.columnIndex = columnIndex;
            this.rect = rect;
        }
    }

    private static class ResultItem {
        private final RequestItem requestItem;
        private final Drawable drawable;

        private ResultItem(RequestItem requestItem, Drawable drawable) {
            this.requestItem = requestItem;
            this.drawable = drawable;
        }
    }

    private class ItemsObserver extends DisposableObserver<List<ResultItem>> {
        @Override
        public void onNext(List<ResultItem> value) {
            super.onNext(value);
            onItemsLoaded(value);
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            Log.e("Mosaic", "error = " + e.getMessage(), e);
            onDataLoadError();
        }
    }
}
