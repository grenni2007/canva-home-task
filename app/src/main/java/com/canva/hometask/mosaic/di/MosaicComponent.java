package com.canva.hometask.mosaic.di;

import com.canva.hometask.mosaic.MosaicActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {MosaicModule.class})
@Singleton
public interface MosaicComponent {
    void inject(MosaicActivity mosaicActivity);
}