package com.canva.hometask.mosaic.util;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;

public class Palette {
    private int averageColor;

    private Palette(int averageColor) {
        this.averageColor = averageColor;
    }

    public static Palette generate(Bitmap bitmap, Rect rect) {
        int averageColor = getAverageColor(bitmap, rect);
        return new Palette(averageColor);
    }

    private static int getAverageColor(Bitmap bitmap, Rect rect) {
//        return getFirstRectPixel(bitmap, rect);
        return calculateAverageColor(bitmap, rect);
    }

    private static int calculateAverageColor(Bitmap bitmap, Rect rect) {
        int width = rect.width() + 1;
        int height = rect.height() + 1;
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, rect.left, rect.top, width, height);
        long redSum = 0;
        long greenSum = 0;
        long blueSum = 0;
        int count = pixels.length;
        for (int pixelColor : pixels) {
            redSum += Color.red(pixelColor);
            greenSum += Color.green(pixelColor);
            blueSum += Color.blue(pixelColor);
        }
        int red = Math.round(redSum / count);
        int green = Math.round(greenSum / count);
        int blue = Math.round(blueSum / count);
        return Color.rgb(red, green, blue);
    }

    @SuppressWarnings("unused")
    private static int getFirstRectPixel(Bitmap bitmap, Rect rect) {
        return bitmap.getPixel(rect.left, rect.top);
    }

    public int getAverageColor() {
        return averageColor;
    }
}
