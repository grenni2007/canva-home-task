package com.canva.hometask.mosaic.presenter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import com.canva.hometask.R;
import com.canva.hometask.mosaic.data_source.BitmapDataSource;
import com.canva.hometask.mosaic.data_source.MosaicDataSource;
import com.canva.hometask.mosaic.router.MosaicRouter;
import com.canva.hometask.mosaic.view.MosaicAdapterImpl;
import com.canva.hometask.mosaic.view.MosaicView;

public class MosaicPresenterImpl implements MosaicPresenter, MosaicView.Listener,
        BitmapDataSource.Listener, MosaicDataSource.Listener {
    private static final int REQUEST_CODE_SELECT_IMAGE = 1050;
    private static final String EXT_IMAGE_URI = "image_uri";
    private MosaicView view;
    private MosaicRouter router;
    private BitmapDataSource bitmapDataSource;
    private MosaicDataSource mosaicDataSource;

    public MosaicPresenterImpl(MosaicView view, MosaicRouter router, BitmapDataSource bitmapDataSource, MosaicDataSource mosaicDataSource) {
        this.view = view;
        this.router = router;
        this.bitmapDataSource = bitmapDataSource;
        this.mosaicDataSource = mosaicDataSource;
    }

    @Override
    public void handleOnCreate(Bundle savedInstanceState) {
        view.initView(this);
        Uri imageUri = savedInstanceState == null ? null : ((Uri) savedInstanceState.getParcelable(EXT_IMAGE_URI));
        if (imageUri != null) {
            onImageUriSelected(imageUri);
        }
    }

    @Override
    public boolean handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri dataUri = data == null ? null : data.getData();
                if (dataUri == null) {
                    view.showError(R.string.image_selection_error);
                } else {
                    onImageUriSelected(dataUri);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private void onImageUriSelected(Uri imageUri) {
        view.resetView();
        bitmapDataSource.cancel();
        mosaicDataSource.cancel();
        bitmapDataSource.loadBitmap(imageUri, view.getMosaicViewWidth(), view.getMosaicViewHeight(), this);
    }

    @Override
    public void handleOnDestroy() {
        view = MosaicView.STUB;
        router = MosaicRouter.STUB;
        bitmapDataSource.cancel();
        bitmapDataSource = BitmapDataSource.STUB;
        mosaicDataSource.cancel();
        mosaicDataSource = MosaicDataSource.STUB;
    }

    @Override
    public void handleSaveInstanceState(Bundle outState) {
        outState.putParcelable(EXT_IMAGE_URI, bitmapDataSource.getSelectedUri());
    }

    @Override
    public void onSelectImageClicked() {
        router.openSelectImageScreen(REQUEST_CODE_SELECT_IMAGE);
    }

    @Override
    public void onRawDataChanged(int i) {
        view.notifyRowChanged(i);
    }

    @Override
    public void onDataItemLoadError() {
        view.showError(R.string.on_item_load_error);
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap) {
        double scaleX = view.getMosaicViewWidth() / (double) bitmap.getWidth();
        double scaleY = view.getMosaicViewHeight() / (double) bitmap.getHeight();
        double scale = Math.min(scaleX, scaleY);

        int viewWidth = (int) Math.round(bitmap.getWidth() * scale);
        int columnCount = viewWidth / view.getItemWidth();
        viewWidth = viewWidth - viewWidth % columnCount;
        int bitmapItemWidth = bitmap.getWidth() / columnCount;

        int viewHeight = (int) Math.round(bitmap.getHeight() * scale);
        int rowCount = viewHeight / view.getItemHeight();
        viewHeight = viewHeight - viewHeight % rowCount;
        int bitmapItemHeight = bitmap.getHeight() / rowCount;

        mosaicDataSource.loadItems(bitmap, rowCount, columnCount, bitmapItemWidth, bitmapItemHeight, this);
        view.updateMosaicView(viewWidth, viewHeight, new MosaicAdapterImpl(mosaicDataSource));
    }

    @Override
    public void onBitmapLoadError(Throwable e) {
        view.showError(R.string.on_bitmap_load_error);
    }
}
