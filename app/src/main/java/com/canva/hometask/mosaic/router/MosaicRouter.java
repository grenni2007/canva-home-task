package com.canva.hometask.mosaic.router;

public interface MosaicRouter {
    MosaicRouter STUB = new MosaicRouter() {
        @Override
        public void openSelectImageScreen(int requestCode) {

        }
    };

    void openSelectImageScreen(int requestCode);
}
