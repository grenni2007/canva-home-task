package com.canva.hometask.mosaic.di;

import android.app.Activity;
import android.content.Context;

import com.canva.hometask.mosaic.data_source.BitmapDataSource;
import com.canva.hometask.mosaic.data_source.BitmapDataSourceImpl;
import com.canva.hometask.mosaic.data_source.MosaicDataSource;
import com.canva.hometask.mosaic.data_source.MosaicDataSourceImpl;
import com.canva.hometask.mosaic.presenter.MosaicPresenter;
import com.canva.hometask.mosaic.presenter.MosaicPresenterImpl;
import com.canva.hometask.mosaic.router.MosaicRouter;
import com.canva.hometask.mosaic.router.MosaicRouterImpl;
import com.canva.hometask.mosaic.util.ImageUtils;
import com.canva.hometask.mosaic.view.MosaicView;
import com.canva.hometask.mosaic.view.MosaicViewImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MosaicModule {
    private final Activity activity;

    public MosaicModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @Singleton
    Activity provideActivity() {
        return activity;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return activity;
    }

    @Provides
    @Singleton
    MosaicPresenter providePresenter(MosaicView view, MosaicRouter router,
                                     BitmapDataSource bitmapDataSource, MosaicDataSource mosaicDataSource) {
        return new MosaicPresenterImpl(view, router, bitmapDataSource, mosaicDataSource);
    }

    @Provides
    @Singleton
    MosaicView provideView(Activity activity) {
        return new MosaicViewImpl(activity);
    }

    @Provides
    @Singleton
    MosaicRouter provideRouter(Activity activity) {
        return new MosaicRouterImpl(activity);
    }

    @Provides
    @Singleton
    BitmapDataSource provideBitmapDataSource(ImageUtils imageUtils) {
        return new BitmapDataSourceImpl(imageUtils);
    }

    @Provides
    @Singleton
    MosaicDataSource provideMosaicDataSource() {
        return new MosaicDataSourceImpl();
    }
}
