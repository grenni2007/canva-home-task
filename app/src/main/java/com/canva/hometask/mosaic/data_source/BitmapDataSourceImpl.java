package com.canva.hometask.mosaic.data_source;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.UiThread;
import android.util.Log;

import com.canva.hometask.base.data_source.DestroyableDataSourceImpl;
import com.canva.hometask.mosaic.util.ImageUtils;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BitmapDataSourceImpl extends DestroyableDataSourceImpl implements BitmapDataSource {
    private final ImageUtils imageUtils;
    private Uri selectedUri;
    private Listener listener;

    public BitmapDataSourceImpl(ImageUtils imageUtils) {
        this.imageUtils = imageUtils;
    }

    @Override
    public void loadBitmap(final Uri dataUri, final int reqWidth, final int reqHeight, Listener listener) {
        selectedUri = dataUri;
        this.listener = listener;
        createBitmapObservable(dataUri, reqWidth, reqHeight)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BitmapObserver());
    }

    @Override
    public void cancel() {
        super.cancel();
        listener = null;
    }

    @Override
    public Uri getSelectedUri() {
        return selectedUri;
    }

    private Observable<Bitmap> createBitmapObservable(final Uri dataUri, final int reqWidth, final int reqHeight) {
        return Observable
                .create(new ObservableOnSubscribe<Bitmap>() {
                    @Override
                    public void subscribe(ObservableEmitter<Bitmap> e) throws Exception {
                        e.onNext(imageUtils.decodeBitmap(dataUri, reqWidth, reqHeight));
                        e.onComplete();
                    }
                });
    }

    private class BitmapObserver extends DisposableObserver<Bitmap> {

        @Override
        @UiThread
        public void onNext(Bitmap bitmap) {
            super.onNext(bitmap);
            listener.onBitmapLoaded(bitmap);
        }

        @Override
        @UiThread
        public void onError(Throwable e) {
            super.onError(e);
            Log.e("Mosaic", "error = " + e.getMessage(), e);
            listener.onBitmapLoadError(e);
        }
    }
}
