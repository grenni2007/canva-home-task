package com.canva.hometask.mosaic.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;

import java.util.HashSet;
import java.util.Set;

public class MosaicGridView extends View {
    private Cache cache;
    private Adapter adapter;

    public MosaicGridView(Context context) {
        this(context, null);
    }

    public MosaicGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MosaicGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(Adapter adapter) {
        if (this.adapter != null) {
            this.adapter.observer = null;
            cache = null;
        }
        this.adapter = adapter;
        if (this.adapter != null) {
            this.adapter.observer = new AdapterDataObserver();
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        cache = null;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (adapter == null) return;
        if (cache == null) {
            cache = new Cache(getWidth(), getHeight());
        } else {
            cache.update();
        }
        canvas.drawBitmap(cache.bitmap, 0, 0, null);
    }

    public static abstract class Adapter {
        private AdapterDataObserver observer;

        public abstract int getRowCount();

        public abstract int getColumnCount();

        protected abstract Drawable getDrawable(int row, int column);

        protected abstract boolean hasAnyDrawables();

        protected abstract boolean isRawHasAllDrawables(int row);

        public void notifyRowChanged(int row) {
            checkMainThread();
            observer.notifyRowChanged(row);
        }

        private void checkMainThread() {
            boolean isMainThread = Thread.currentThread() == Looper.getMainLooper().getThread();
            if (!isMainThread) {
                throw new IllegalStateException("please use UI thread");
            }
        }
    }

    private class AdapterDataObserver {
        private void notifyRowChanged(int row) {
            if (cache != null) {
                cache.notifyRowChanged(row);
            }
            if (adapter.isRawHasAllDrawables(row)) {
                invalidate();
            }
        }
    }

    private class Cache {
        final Canvas canvas;
        final Bitmap bitmap;
        Set<Integer> changedRows = new HashSet<>();

        private Cache(int width, int height) {
            Bitmap.Config conf = Bitmap.Config.ARGB_8888;
            bitmap = Bitmap.createBitmap(width, height, conf);
            canvas = new Canvas(bitmap);
            invalidate();
        }

        private void invalidate() {
            if (!adapter.hasAnyDrawables()) return;
            for (int i = 0; i < adapter.getRowCount(); i++) {
                for (int j = 0; j < adapter.getColumnCount(); j++) {
                    drawItem(canvas, i, j);
                }
            }
        }

        private void drawItem(Canvas canvas, int i, int j) {
            Drawable drawable = adapter.getDrawable(i, j);
            if (drawable == null) return;
            int left = getItemWidth() * j;
            int top = i * getItemHeight();
            int right = left + getItemWidth();
            int bottom = top + getItemHeight();
            drawable.setBounds(new Rect(left, top, right, bottom));
            drawable.draw(canvas);
        }

        private int getItemWidth() {
            return getWidth() / adapter.getColumnCount();
        }

        private int getItemHeight() {
            return getHeight() / adapter.getRowCount();
        }

        private void notifyRowChanged(int row) {
            changedRows.add(row);
        }

        private void update() {
            for (int row : changedRows) {
                for (int j = 0; j < adapter.getColumnCount(); j++) {
                    drawItem(canvas, row, j);
                }
            }
            changedRows.clear();
        }
    }
}
