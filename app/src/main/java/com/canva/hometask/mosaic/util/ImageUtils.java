package com.canva.hometask.mosaic.util;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.FileNotFoundException;

import javax.inject.Inject;

public class ImageUtils {
    private final ContentResolver contentResolver;

    @Inject
    ImageUtils(Context context) {
        contentResolver = context.getContentResolver();
    }

    public Bitmap decodeBitmap(Uri dataUri, int reqWidth, int reqHeight) throws FileNotFoundException {
        int inSampleSize = calculateInSampleSize(decodeBounds(dataUri), reqWidth, reqHeight);
        return decodeBitmap(dataUri, inSampleSize);
    }

    private Bitmap decodeBitmap(Uri dataUri, int inSampleSize) throws FileNotFoundException {
        BitmapFactory.Options options;
        options = new BitmapFactory.Options();
        options.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(contentResolver.openInputStream(dataUri), null, options);
    }

    @NonNull
    private BitmapFactory.Options decodeBounds(Uri dataUri) throws FileNotFoundException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(contentResolver.openInputStream(dataUri), null, options);
        return options;
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        if (reqWidth == 0 || reqHeight == 0) return 1;
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            while ((height / inSampleSize) > reqHeight
                    || (width / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
}
