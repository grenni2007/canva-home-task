package com.canva.hometask.mosaic.data_source;

import android.graphics.Bitmap;
import android.net.Uri;

import com.canva.hometask.base.data_source.DestroyableDataSource;

public interface BitmapDataSource extends DestroyableDataSource {
    BitmapDataSource STUB = new BitmapDataSource() {
        @Override
        public void loadBitmap(Uri dataUri, int reqWidth, int reqHeight, Listener listener) {

        }

        @Override
        public Uri getSelectedUri() {
            return null;
        }

        @Override
        public void cancel() {

        }
    };

    void loadBitmap(Uri dataUri, int reqWidth, int reqHeight, Listener listener);

    Uri getSelectedUri();

    interface Listener {
        void onBitmapLoaded(Bitmap bitmap);

        void onBitmapLoadError(Throwable e);
    }
}
